package com.example.appubi;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Notifi extends AppCompatActivity {
    private Button esp, gen, back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifi);
        esp = (Button) findViewById(R.id.button4);
        gen = (Button) findViewById(R.id.button5);
        back = (Button) findViewById(R.id.button6);

        FirebaseMessaging.getInstance().subscribeToTopic("atodos").addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Toast.makeText(Notifi.this, "Se agrego a la lista", Toast.LENGTH_SHORT).show();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Notifi.this, MainActivity.class);
                startActivity(intent);
            }
        });
        gen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llamarGeneral();
            }
        });

    }

    private void llamarGeneral(){
        RequestQueue myrequest = Volley.newRequestQueue(getApplicationContext());
        JSONObject json = new JSONObject();
        try {
            json.put("to","/topics/"+"atodos");
            JSONObject notificaciones = new JSONObject();
            notificaciones.put("titulo", "Hola");
            notificaciones.put("detalle","Vato");
            json.put("data",notificaciones);
            String URL = "https://fcm.googleapis.com/fcm/send";
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,URL,json,null, null){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> header = new HashMap<>();
                    header.put("content-type","application/json");
                    header.put("authorization","key=AAAA0BSlSlY:APA91bEGlmxjLB3ltON5gCqJSssoCIJ28sSywFLG1xAEcH-KP4IEDIIxYDnV0dfV7jvkVM5L-I7GGCEObq2k0rWjYcUzIEHyD4NabzTORcESubh_zdxqa26g52_KeotZ53OQIfi9UTOi");
                    return header;
                }
            };
            myrequest.add(request);
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    private void llamarEspe(){
        RequestQueue myrequest = Volley.newRequestQueue(getApplicationContext());
        JSONObject json = new JSONObject();
        try {

            String token="";

            json.put("to",token);
            JSONObject notificaciones = new JSONObject();
            notificaciones.put("titulo", "Hola");
            notificaciones.put("detalle","Vato");
            json.put("data",notificaciones);
            String URL = "https://fcm.googleapis.com/fcm/send";
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,URL,json,null, null){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> header = new HashMap<>();
                    header.put("content-type","application/json");
                    header.put("authorization","key=AAAA0BSlSlY:APA91bEGlmxjLB3ltON5gCqJSssoCIJ28sSywFLG1xAEcH-KP4IEDIIxYDnV0dfV7jvkVM5L-I7GGCEObq2k0rWjYcUzIEHyD4NabzTORcESubh_zdxqa26g52_KeotZ53OQIfi9UTOi");
                    return header;
                }
            };
            myrequest.add(request);
        }catch (JSONException e){
            e.printStackTrace();
        }
    }
}
