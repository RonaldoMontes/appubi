package com.example.appubi;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import android.location.Location;
import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    private FusedLocationProviderClient fusedLocationClient;
    DatabaseReference BD;
    private Button regFire;
    private Button Ubic, Dis,Reg;
    private EditText eled, L1, Ln1, L2, Ln2;
    private ListView Lista;

    private int MY_PERMISSIONS_REQUEST_READ_CONTACTS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        regFire = (Button) findViewById(R.id.button);
        Ubic = (Button) findViewById(R.id.button2);
        eled = (EditText) findViewById(R.id.editText);
        Lista = (ListView) findViewById(R.id.ListView);
        L1 = (EditText) findViewById(R.id.editText2);
        Ln1 = (EditText) findViewById(R.id.editText6);
        L2 = (EditText) findViewById(R.id.editText4);
        Ln1 = (EditText) findViewById(R.id.editText5);
        Dis = (Button) findViewById(R.id.button3);
        Reg=(Button)findViewById(R.id.button7);



        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        BD = FirebaseDatabase.getInstance().getReference();
        Mostrar();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_READ_CONTACTS);

            return;
        }
        Reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Notifi.class);
                startActivity(intent);
            }
        });
        regFire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ObtenerUbi();
            }
        });
        Ubic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MapsActivity.class);
                startActivity(intent);
            }
        });

        Dis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calcular();
            }
        });
    }

    private void Calcular(){
        Location locationA = new Location("punto A");
        double la1 = Double.parseDouble(L1.getText().toString());
        double ln1 = Double.parseDouble(Ln1.getText().toString());
        double la2 = Double.parseDouble(L2.getText().toString());
        double ln2 = Double.parseDouble(L1.getText().toString());
        locationA.setLatitude(la1);
        locationA.setLongitude(ln1);

        Location locationB = new Location("punto B");

        locationB.setLatitude(la2);
        locationB.setLongitude(ln2);

        double distance = locationA.distanceTo(locationB);
        Toast.makeText(MainActivity.this, "La distancia es" + distance + ".Km" , Toast.LENGTH_SHORT).show();

    }


    private void Mostrar() {
        BD.child("usuarios").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    String cad = "";
                    ArrayList<String> lope = new ArrayList<>();
                for (DataSnapshot snapshot: dataSnapshot.getChildren()  ){
                    UbicTodos lol = snapshot.getValue(UbicTodos.class);

                    String nombre = lol.getNombre();
                    Double latitud= lol.getLatitud();
                    Double longitud = lol.getLongitud();
                  cad = cad + "Nombre: " + nombre +"\n Latitud: " + latitud +"\n Longitud: "+  longitud + "\n";
                }
                lope.add(cad);
                ArrayAdapter adaptador = new ArrayAdapter(MainActivity.this, android.R.layout.simple_list_item_1, lope);
                Lista.setAdapter(adaptador);


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void ObtenerUbi() {

        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        String ed = eled.getText().toString();
                        if (location != null) {
                            Log.e("Latitud:",+location.getLatitude() + "Longitud: " +location.getLongitude());

                            Map<String,Object> posicion = new HashMap<>();
                            posicion.put("nombre", ed);
                            posicion.put("latitud", location.getLatitude());
                            posicion.put("longitud", location.getLongitude());
                            BD.child("usuarios").push().setValue(posicion);
                        }
                    }
                });
    }


}
